#version 410
layout(location=0) in vec4 Position;
layout(location=1) in vec2 TexCoord;
layout(location=3) in vec4 Normal;

out vec4 vNormal;
out vec2 vTexCoord;
out vec4 vPosition;

uniform mat4 ProjectionView;
uniform mat4 ModelTransform;

void main()
{
	vTexCoord = TexCoord;
	vPosition = Position;
	vNormal = Normal;
	gl_Position = ProjectionView * ModelTransform * Position;
}