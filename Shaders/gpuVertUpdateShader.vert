#version 410

layout(location=0) in vec3 Position;
layout(location=1) in vec3 Velocity;
layout(location=2) in float Lifetime;
layout(location=3) in float Lifespan;

out vec3 position;
out vec3 velocity;
out float lifetime;
out float lifespan;

uniform float time;
uniform float deltaTime;
uniform float lifeMin;
uniform float lifeMax;
uniform float maxVelocity;
uniform vec3 emitterPosition;

const float INVERSE_MAX_UINT = 1.0f / 4294967295.0f;

float rand(uint seed, float range)
{
	uint i = (seed ^ 12345391u) * 2654435769u;
	i ^= (i << 6u) ^ (i >> 26u);
	i *= 2654435769u;
	i += (i << 5u) ^ (i >> 12u);
	return float(range * i) * INVERSE_MAX_UINT;
}

vec3 newVelocity = vec3(0);

void main()
{
	vec3 forward = emitterPosition - Position;
	vec3 up = vec3(0,1,0);
	vec3 right = cross(normalize(forward), normalize(up));

	newVelocity = Velocity;
	
	//Warp velocity
	//float a = Lifetime, b = Lifespan;
	//newVelocity.x = (a-b) * sin(time) + b * sin(time * ((a/b) - 1));
	//newVelocity.y = (a-b) * sin(time) / b * cos(time * ((a/b) - 1));
	//newVelocity.z = (a-b) * cos(time) - b * cos(time * ((a/b) - 1));

	vec3 target = vec3(sin(time), cos(time), -tan(time));
	vec3 direction = normalize(target - Position);
	newVelocity += direction * 0.05f;
	
	
	
	
	position = Position + Velocity * deltaTime;
	velocity = newVelocity;
	lifetime = Lifetime + deltaTime;
	lifespan = Lifespan;
	
	uint seed = uint(time * 1000.0f) + uint(gl_VertexID);

	//Emit new particle as one dies
	if(lifetime > lifespan)
	{
		//black hole
		//velocity.x = rand(seed++, 2) / maxVelocity + sin(time);
		//velocity.y = rand(seed++, 2) / maxVelocity + cos(time);
		//velocity.z = rand(seed++, 2) / maxVelocity + tan(time);

		//Morph
		velocity.x = rand(seed++, 2) - 1;
		velocity.y = rand(seed++, 2) - 1;
		velocity.z = rand(seed++, 2) - 1;

		velocity = normalize(velocity);
		position = emitterPosition + vec3(sin(time) * 0.5f, sin(time) * 0.5f, cos(time) * 0.5f);
		lifetime = 0;
		lifespan = rand(seed++, lifeMax - lifeMin) + lifeMin;
	}
}