//texture
#version 410
in vec2 vTexCoord;
in vec4 vNormal;
in vec4 vPosition;

out vec4 FragColor;

uniform sampler2D diffuse;
uniform vec3 LightDir;
uniform vec3 LightColour;
uniform vec3 CameraPos;
uniform float SpecPow;

void main() 
{
	float d = max(0, dot( normalize(vNormal.xyz), LightDir ) );
	vec3 E = normalize( CameraPos - vPosition.xyz );
	vec3 R = reflect ( -LightDir, vNormal.xyz);
	float s = max(0, dot(E,R));
	s = pow(s,SpecPow);
	FragColor = texture(diffuse,vTexCoord) * (vec4(LightColour,1) * d + vec4(LightColour,1) * s);
	FragColor.a = 1;
}






//FBX
//#version 410
//in vec4 vNormal;
//out vec4 FragColor;
//void main() 
//{
//FragColor = vec4(1,1,1,1);
//}

//lighting
//#version 410
//in vec4 vNormal;
//in vec2 vTexCoord;
//out vec4 FragColor;
//uniform sampler2D diffuse;
//void main()
//{
//float d = max(0, dot( normalize(vNormal.xyz), vec3(0,1,0) ) );
////FragColor = vec4(d,d,d,1);
//FragColor = texture(diffuse,vTexCoord) * d;
//FragColor.a = 1;
//}