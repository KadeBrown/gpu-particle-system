#include "Application.h"
#include "FlyCamera.h"
#include "DeltaTime.h"
#include <iostream>


void FlyCamera::Update()
{
	GetInput();
}

void FlyCamera::GetInput()
{
	mat4 transform = GetWorldTransform();

	vec3 right	 = vec3(transform[0].x, transform[0].y, transform[0].z);
	vec3 forward = vec3(transform[2].x, transform[2].y, transform[2].z);
	vec3 moveDirection(0);

	if (glfwGetKey(APPLICATION.GetWindow(), GLFW_KEY_W) == GLFW_PRESS)
		moveDirection -= forward;
	if (glfwGetKey(APPLICATION.GetWindow(), GLFW_KEY_A) == GLFW_PRESS)
		moveDirection -= right;
	if (glfwGetKey(APPLICATION.GetWindow(), GLFW_KEY_S) == GLFW_PRESS)
		moveDirection += forward;
	if (glfwGetKey(APPLICATION.GetWindow(), GLFW_KEY_D) == GLFW_PRESS)
		moveDirection += right;

	if (glfwGetKey(APPLICATION.GetWindow(), GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
		SetSpeed(m_InitSpeed * m_SpeedMultiplier);
	else
		SetSpeed(m_InitSpeed);


	float length = glm::length(moveDirection);

	if (length > 0.01f)
	{
		moveDirection = glm::normalize(moveDirection) * m_Speed * TIME.GetDeltaTime();
		SetPosition(vec3(GetWorldTransform()[3]) + moveDirection);
	}

	if (glfwGetMouseButton(APPLICATION.GetWindow(), GLFW_MOUSE_BUTTON_2) == GLFW_PRESS)
	{
		glfwSetInputMode(APPLICATION.GetWindow(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

		if (!m_MouseClicked)
		{
			int width;
			int height;

			glfwGetFramebufferSize(APPLICATION.GetWindow(), &width, &height);

			//Calculate center of the window
			m_CursorX = (float)width / 2;
			m_CursorY = (float)height / 2;

			//Move cursor to center of the window;
			glfwSetCursorPos(APPLICATION.GetWindow(), width / 2, height / 2);

			m_MouseClicked = true;
		}
		else
		{

			double mouseX;
			double mouseY;

			glfwGetCursorPos(APPLICATION.GetWindow(), &mouseX, &mouseY);

			//Calculate the distance from the cursor to the center of the window
			double xOffset = mouseX - m_CursorX;
			double yOffset = mouseY - m_CursorY;

			CalculateRotation(xOffset, yOffset);
		}

		//Move Cursor back to center of the window ready for the next frames calculation
		int width;
		int height;

		glfwGetFramebufferSize(APPLICATION.GetWindow(), &width, &height);
		glfwSetCursorPos(APPLICATION.GetWindow(), width / 2, height / 2);
	}
	else
	{
		m_MouseClicked = false;
		glfwSetInputMode(APPLICATION.GetWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}
}

void FlyCamera::GetFrustumPlanes(const mat4& transform, vec4* planes)
{
	//Right plane
	planes[0] = vec4(transform[0][3] - transform[0][0],
					 transform[1][3] - transform[1][0],
					 transform[2][3] - transform[2][0],
					 transform[3][3] - transform[3][0]);
	//Left plane
	planes[1] = vec4(transform[0][3] + transform[0][0],
					 transform[1][3] + transform[1][0],
					 transform[2][3] + transform[2][0],
					 transform[3][3] + transform[3][0]);
	//Top plane
	planes[2] = vec4(transform[0][3] - transform[0][1],
					 transform[1][3] - transform[1][1],
					 transform[2][3] - transform[2][1],
					 transform[3][3] - transform[3][1]);
	//Bottom plane
	planes[3] = vec4(transform[0][3] + transform[0][1],
					 transform[1][3] + transform[1][1],
					 transform[2][3] + transform[2][1],
					 transform[3][3] + transform[3][1]);
	//Far plane
	planes[4] = vec4(transform[0][3] - transform[0][2],
					 transform[1][3] - transform[1][2],
					 transform[2][3] - transform[2][2],
					 transform[3][3] - transform[3][2]);
	//Near plane
	planes[5] = vec4(transform[0][3] + transform[0][2],
					 transform[1][3] + transform[1][2],
					 transform[2][3] + transform[2][2],
					 transform[3][3] + transform[3][2]);
}

void FlyCamera::CalculateRotation(double xOffset, double yOffset)
{
	if (xOffset == 0 && yOffset == 0)
		return;

	//If the mouse has moved
	//Calculate 'x' rotation
	if (xOffset != 0.0)
	{
		mat4 rotation = glm::rotate((float)(m_Sensitivity * -xOffset), vec3(0, 1, 0));
		SetTransform(GetWorldTransform() * rotation);
	}

	//Calculate 'y' rotation
	if (yOffset != 0.0)
	{
		mat4 rotation = glm::rotate((float)(m_Sensitivity * -yOffset), vec3(1, 0, 0));
		SetTransform(GetWorldTransform() * rotation);
	}

	mat4 transform = GetWorldTransform();

	mat4 newTransform;
	vec3 worldUp = vec3(0, 1, 0);

	//Calculate new forward vector
	vec3 forward = vec3(transform[2].x, transform[2].y, transform[2].z);
	newTransform[0] = glm::normalize(vec4(glm::cross(worldUp, forward), 0));

	//Calculate new right vector
	vec3 newRight = vec3(transform[0].x, transform[0].y, transform[0].z);
	newTransform[1] = glm::normalize(vec4(glm::cross(forward, newRight), 0));

	//Keep position data from old transform
	newTransform[2] = glm::normalize(transform[2]);
	newTransform[3] = transform[3];

	SetTransform(newTransform);

}
