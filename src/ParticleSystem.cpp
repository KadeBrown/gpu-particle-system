#include "ParticleSystem.h"
#include "gl_core_4_4.h"
#include <iostream>
#include <string>
#include <fstream>



ParticleSystem::ParticleSystem()
	: m_Particles(nullptr), m_MaxParticles(0),
	  m_Position(0,0,0), m_DrawShader(0),
	  m_UpdateShader(0), m_LastDrawTime(0)
{
	m_VAO[0] = 0;
	m_VAO[1] = 0;
	m_VBO[0] = 0;
	m_VBO[1] = 0;
}


ParticleSystem::~ParticleSystem()
{
	delete[] m_Particles;

	glDeleteVertexArrays(2, m_VAO);
	glDeleteBuffers(2, m_VBO);

	//Delete shaders
	glDeleteProgram(m_DrawShader);
	glDeleteProgram(m_UpdateShader);
}

void ParticleSystem::Initialise(unsigned int maxPrticles, float lifespanMin, 
	float lifespanMax, float velocityMin, float velocityMax, float startSize, 
	float endSize, const vec4 & startColour, const vec4 & endColour)
{
	m_MaxParticles = maxPrticles;
	m_LifespanMin = lifespanMin;
	m_LifespanMax = lifespanMax;
	m_VelocityMin = velocityMin;
	m_VelocityMax = velocityMax;
	m_StartSize	  = startSize;
	m_EndSize	  = endSize;
	m_StartColour = startColour;
	m_EndColour   = endColour;

	//Create particle array
	m_Particles = new Particle[m_MaxParticles];

	//Set Ping-Pong buffer
	m_ActiveBuffer = 0;

	CreateBuffers();
	CreateUpdateShader();
	CreateDrawShader();
}

void ParticleSystem::Draw(float time, const mat4 & cameraTransform, const mat4 & projectionView)
{
	//Update the particles using transform feedback
	glUseProgram(m_UpdateShader);

	//Bind time information
	int location = glGetUniformLocation(m_UpdateShader, "time");
	glUniform1f(location, time);

	float deltaTime = time - m_LastDrawTime;
	m_LastDrawTime = time;

	location = glGetUniformLocation(m_UpdateShader, "deltaTime");
	glUniform1f(location, deltaTime);

	location = glGetUniformLocation(m_UpdateShader, "maxVelocity");
	glUniform1f(location, m_VelocityMax);

	//Bind emitters position
	location = glGetUniformLocation(m_UpdateShader, "emitterPosition");
	glUniform3fv(location, 1, &m_Position[0]);

	//Disable rasterisation
	glEnable(GL_RASTERIZER_DISCARD);

	//Bind the buffer we will update
	glBindVertexArray(m_VAO[m_ActiveBuffer]);

	//Work out the second buffer
	unsigned int secondBuffer = (m_ActiveBuffer + 1) % 2;

	//Bind buffer we will update into as points and begin transform feedback
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, m_VBO[secondBuffer]);
	glBeginTransformFeedback(GL_POINTS);
	glDrawArrays(GL_POINTS, 0, m_MaxParticles);

	//Disable transform feedback and enable rasterisation
	glEndTransformFeedback();
	glDisable(GL_RASTERIZER_DISCARD);
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);

	//Draw the particles using geometry shader
	glUseProgram(m_DrawShader);

	location = glGetUniformLocation(m_DrawShader, "projectionView");
	glUniformMatrix4fv(location, 1, false, &projectionView[0][0]);

	location = glGetUniformLocation(m_DrawShader, "cameraTransform");
	glUniformMatrix4fv(location, 1, false, &cameraTransform[0][0]);

	//Draw particles in second buffer
	glBindVertexArray(m_VAO[secondBuffer]);
	glDrawArrays(GL_POINTS, 0, m_MaxParticles);

	//Swap for next frame
	m_ActiveBuffer = secondBuffer;
}

unsigned int ParticleSystem::LoadShader(unsigned int type, const char * filePath)
{
	FILE* file = fopen(filePath, "rb");
	if (file == nullptr)
		return 0;

	//Read shader
	fseek(file, 0, SEEK_END);
	unsigned int length = ftell(file);
	fseek(file, 0, SEEK_SET);
	char* source = new char[length + 1];
	memset(source, 0, length + 1);
	fread(source, sizeof(char), length, file);
	fclose(file);

	unsigned int shader = glCreateShader(type);
	glShaderSource(shader, 1, &source, 0);
	glCompileShader(shader);
	delete[] source;
	return shader;
}

void ParticleSystem::CreateBuffers()
{
	//Create Buffers
	glGenVertexArrays(2, m_VAO);
	glGenBuffers(2, m_VBO);

	//Setup first buffer
	glBindVertexArray(m_VAO[0]);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO[0]);
	glBufferData(GL_ARRAY_BUFFER, m_MaxParticles * sizeof(Particle), m_Particles, GL_STREAM_DRAW);

	glEnableVertexAttribArray(0); //Position
	glEnableVertexAttribArray(1); //Velocity
	glEnableVertexAttribArray(2); //Lifetime
	glEnableVertexAttribArray(3); //Lifespan

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), ((char*)0) + 12);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), ((char*)0) + 24);
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), ((char*)0) + 28);

	//Setup second buffer
	glBindVertexArray(m_VAO[1]);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO[1]);
	glBufferData(GL_ARRAY_BUFFER, m_MaxParticles * sizeof(Particle), 0, GL_STREAM_DRAW);

	glEnableVertexAttribArray(0); //Position
	glEnableVertexAttribArray(1); //Velocity
	glEnableVertexAttribArray(2); //Lifetime
	glEnableVertexAttribArray(3); //Lifespan

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), ((char*)0) + 12);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), ((char*)0) + 24);
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), ((char*)0) + 28);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


}

void ParticleSystem::CreateUpdateShader()
{
	//Create Shader
	unsigned int vertShader = LoadShader(GL_VERTEX_SHADER, "Shaders/gpuVertUpdateShader.vert");
	m_UpdateShader = glCreateProgram();
	glAttachShader(m_UpdateShader, vertShader);

	//Specify data we will stream back
	const char* varyings[] = { "position", "velocity", "lifetime", "lifespan" };
	glTransformFeedbackVaryings(m_UpdateShader, 4, varyings, GL_INTERLEAVED_ATTRIBS);

	glLinkProgram(m_UpdateShader);

	//Remove un-needed handles
	glDeleteShader(vertShader);

	glUseProgram(m_UpdateShader);

	//Bind lifetime
	int location = glGetUniformLocation(m_UpdateShader, "lifeMin");
	glUniform1f(location, m_LifespanMin);

	location = glGetUniformLocation(m_UpdateShader, "lifeMax");
	glUniform1f(location, m_LifespanMax);
}

void ParticleSystem::CreateDrawShader()
{
	unsigned int vertShader = LoadShader(GL_VERTEX_SHADER, "Shaders/gpuVertShader.vert");
	unsigned int geomShader = LoadShader(GL_GEOMETRY_SHADER, "Shaders/gpuGeometryShader.geom");
	unsigned int fragShader = LoadShader(GL_FRAGMENT_SHADER, "Shaders/gpuFragShader.frag");


	m_DrawShader = glCreateProgram();
	glAttachShader(m_DrawShader, vertShader);
	glAttachShader(m_DrawShader, fragShader);
	glAttachShader(m_DrawShader, geomShader);
	glLinkProgram(m_DrawShader);

	//Remove un-needed handles
	glDeleteShader(vertShader);
	glDeleteShader(geomShader);
	glDeleteShader(fragShader);

	//Bind shader so that we can set uniforms
	glUseProgram(m_DrawShader);

	//Bind size information for interpolation
	int location = glGetUniformLocation(m_DrawShader, "sizeStart");
	glUniform1f(location, m_StartSize);

	location = glGetUniformLocation(m_DrawShader, "sizeEnd");
	glUniform1f(location, m_EndSize);

	//Bind colour information for interpolation
	location = glGetUniformLocation(m_DrawShader, "colourStart");
	glUniform4fv(location, 1, &m_StartColour[0]);

	location = glGetUniformLocation(m_DrawShader, "colourEnd");
	glUniform4fv(location, 1, &m_EndColour[0]);

}
