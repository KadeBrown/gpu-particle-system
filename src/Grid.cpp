#include "Application.h"
#include "Grid.h"
#include "Texture.h"
#include "Program.h"




Grid::Grid()
{
	m_Texture = new Texture();
	m_Program = new Program();

	m_Rows = 0;
	m_Cols = 0;
	m_Dims = 64;
	m_Amplitude = 85;
	m_Persistance = 0.6f;
	m_Scale = (1.0f / m_Dims) * 3;
	m_Octaves = 6;
	m_Seed = 0;
}


Grid::~Grid()
{
	//Unbind Buffers
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Grid::GenerateGrid(unsigned int rows, unsigned int cols, const char* vertShaderPath, 
							const char* fragShaderPath, const char* texturePath)
{
	m_Program->CreateNewProgram(vertShaderPath, fragShaderPath);
	if(texturePath != nullptr || texturePath != "")
		m_Texture->LoadTexture(texturePath);

	m_Rows = rows;
	m_Cols = cols;
	m_TotalRows = rows;
	m_TotalCols = cols;

	//GeneratePerlinData();

	const float rowSpacing = 1;
	const float colSpacing = 1;
	const float totalRowSize = rows * rowSpacing;
	const float totalColSize = cols * colSpacing;

	//Create an array to hold data for each vertex
	Vertex* vertices = new Vertex[rows * cols];

	//loop through rows
	for (unsigned int rowIndex = 0; rowIndex < rows; ++rowIndex)
	{
		//loop through columns
		for (unsigned int colIndex = 0; colIndex < cols; ++colIndex)
		{
			unsigned int vertIndex = rowIndex * cols + colIndex;

			//Set and save data for each vertex
			vertices[vertIndex].v_Position  = vec4((float)colIndex * colSpacing, 0, (float)rowIndex * rowSpacing, 1);
			vertices[vertIndex].v_Color		= vec4(0, 0.25f, 1, 1);
			vertices[vertIndex].v_TexCoord  = vec2(vertices[vertIndex].v_Position.x / totalColSize, vertices[vertIndex].v_Position.z / totalRowSize);
		}
	}

	//Save all indices without duplicating the same indice
	unsigned int* indices = new unsigned int[(rows - 1) * (cols - 1) * 6];
	unsigned int  index	  = 0;

	for (unsigned int rowIndex = 0; rowIndex < rows - 1; ++rowIndex)
	{
		for (unsigned int colIndex = 0; colIndex < cols - 1; ++colIndex)
		{
			//Saves three vertices that make up a triangle with each square on grid
			//Triangle 1
			indices[index++] = rowIndex * cols + colIndex;
			indices[index++] = (rowIndex + 1) * cols + colIndex;
			indices[index++] = (rowIndex + 1) * cols + (colIndex + 1);

			//Triangle 2
			indices[index++] = rowIndex * cols + colIndex;
			indices[index++] = rowIndex * cols + (colIndex + 1);
			indices[index++] = (rowIndex + 1) * cols + (colIndex + 1);
		}
	}

	//Multiply by 6 as there are six indices (2 triangles) in each square
	m_IndiceCount = (rows - 1) * (cols - 1) * 6;

	//Create ID for buffer and bind buffers to VAO (Vertex Array Object)
	glGenBuffers(1, &m_VBO);
	glGenBuffers(1, &m_IBO);
	glGenVertexArrays(1, &m_VAO);

	glBindVertexArray(m_VAO);

	//Select buffer
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);

	//Allocates necessary memory and copies vertex data
	glBufferData(GL_ARRAY_BUFFER, (rows * cols) * sizeof(Vertex), vertices, GL_STATIC_DRAW);

	//Enables vertex data slots
	glEnableVertexAttribArray(0); //v_Position
	glEnableVertexAttribArray(1); //v_Color
	glEnableVertexAttribArray(2); //v_Normal
	glEnableVertexAttribArray(3); //v_TexCoord

	//Notify open_GL how data has been constructed
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, v_Position));
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, v_Color));
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_TRUE , sizeof(Vertex), (void*)offsetof(Vertex, v_Normal));
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, v_TexCoord));

	//Element array is specifically for indices
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_IndiceCount * sizeof(unsigned int), indices, GL_STATIC_DRAW);

	//Clear Memory
	delete[] vertices;
	delete[] indices;
}

void Grid::Render()
{
	//Select program to use
	glUseProgram(m_Program->GetProgramID());

	//Set variable values in shader
	int location = glGetUniformLocation(m_Program->GetProgramID(), "ProjectionView");
	glUniformMatrix4fv(location, 1, GL_FALSE, &(APPLICATION.GetFlyCamera()->GetProjectionView()[0][0]));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_Texture->GetTexture());
	location = glGetUniformLocation(m_Program->GetProgramID(), "diffuse");
	glUniform1i(location, 0);

	glBindVertexArray(m_VAO);
	glDrawElements(GL_TRIANGLES, m_IndiceCount, GL_UNSIGNED_INT, 0);

	glBindTexture(GL_TEXTURE_2D, 0);

}

void Grid::GeneratePerlinData()
{

}
