#include "Application.h"
#include "Texture.h"
#include "stb-master\stb_image.h"


void Texture::LoadTexture(const char* textureLocation)
{
	if (textureLocation == nullptr)
		return;

	m_TextureLocation = textureLocation;

	m_Data = stbi_load(textureLocation, &m_ImageWidth, &m_ImageHeight, &m_ImageFormat, 3);

	glGenTextures(1, &m_Texture);
	glBindTexture(GL_TEXTURE_2D, m_Texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_ImageWidth, m_ImageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, m_Data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	stbi_image_free(m_Data);
}
